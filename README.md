# rust-datastreamcorelib

Rust version of https://gitlab.com/advian-oss/python-datastreamcorelib

NOTE: 1.0 is API incompatible with <1.0 versions due to starting to use
parking_lot mutexes for synchronization.

## Docker

Remember to add any new system packages you install via the devel\_shell
to the Dockerfile too. Easy way to check if you remembered everything is
to rebuild the test image and run it (see below).

We need [buildkit](https://docs.docker.com/develop/develop-images/build_enhancements/):

    export DOCKER_BUILDKIT=1

### Creating the container

Build image, create container and start it:

    docker build --ssh default --target devel_shell -t rustdatastreamcorelib:devel_shell .
    docker create --name rustdatastreamcorelib_devel -v `pwd`":/app" -it  rustdatastreamcorelib:devel_shell
    docker start -i rustdatastreamcorelib_devel

This will give you a shell with system level dependencies installed, you
should do any shell things (like run tests, pre-commit checks etc)
there.

### pre-commit considerations

If working in Docker instead of native env you need to run the
pre-commit checks in docker too:

    docker exec -i rustdatastreamcorelib_devel /bin/bash -c "pre-commit install"
    docker exec -i rustdatastreamcorelib_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use
the docker run syntax but using the running container is faster:

    docker run --rm -it -v `pwd`":/app" rustdatastreamcorelib:devel_shell -c "pre-commit run --all-files"

### Test suite

You can use the devel shell to run cargo test when doing development, for
CI use the \"test\" target in the Dockerfile:

    docker build --ssh default --target test -t rustdatastreamcorelib:test .
    docker run --rm --security-opt seccomp=unconfined -it -v `pwd`":/app"  rustdatastreamcorelib:test
