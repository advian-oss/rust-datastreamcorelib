/// Marker traits
use crate::abstracts::ZMQCodec;
use std::fmt;

/// Marker trait for all ZMQ messages
pub trait ZMQMessageMarker: ZMQCodec + Clone + fmt::Debug {}

/// Marker trait for things that are compatible with PubSubMessage
pub trait PubSubMessageMarker: ZMQMessageMarker {}

/// Marker trait for things that are compatible with DataMessage
pub trait DataMessageMarker: ZMQMessageMarker {}

/// Marker trait for things that are compatible with PubSubDataMessage
pub trait PubSubDataMessageMarker: PubSubMessageMarker + DataMessageMarker {}

/// Marker trait for things that are compatible with ImageMessage
pub trait ImageMessageMarker: DataMessageMarker {}

/// Marker trait for things that are compatible with PubSubImageMessage
pub trait PubSubImageMessageMarker:
    PubSubMessageMarker + ImageMessageMarker + PubSubDataMessageMarker
{
}
