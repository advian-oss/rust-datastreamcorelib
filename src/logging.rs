/// Logging related helpers
use chrono;
use fern;
use log;
use std::io;

/// Initialize logging with our standard output style to stderr
pub fn init_logging(loglevel: log::LevelFilter) -> Result<(), fern::InitError> {
    let base_config = fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}] {} {}:{} | {}",
                chrono::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, true),
                record.level(),
                record.target(),
                match record.file() {
                    Some(f) => f,
                    None => "<nofile>",
                },
                match record.line() {
                    Some(l) => l.to_string(),
                    None => "0".to_string(),
                },
                message
            ))
        })
        .level(loglevel);

    // Send all log messages to stderr (same as python)
    let stdout_config = fern::Dispatch::new().chain(io::stderr());
    base_config.chain(stdout_config).apply()?;

    Ok(())
}

/// Initialize logging from integer verbosity level 0=Warn and ups from there to 3=Trace
pub fn init_logging_from_verbosity(verbosity: u64) -> Result<(), fern::InitError> {
    let loglevel = match verbosity {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        3 => log::LevelFilter::Trace,
        _ => {
            panic!("Invalid value");
        }
    };
    init_logging(loglevel)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_signature() {
        init_logging_from_verbosity(3).unwrap();
        log::error!("Test error");
        log::warn!("Test warning");
        log::debug!("Test debug");
        log::trace!("Test trace");
        // TODO: how to capture output and verify it matches expected.
    }
}
