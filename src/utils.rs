/// Utility functions
use crate::datamessage::PubSubDataMessage;
use crate::resources::Usage;
use failure::Fallible;
use serde_json;

/// Helper to create a heartbeat message
pub fn create_heartbeat_message() -> Fallible<PubSubDataMessage> {
    let mut msg = PubSubDataMessage::new("HEARTBEAT".to_string())?;
    msg.data["resources"] = serde_json::to_value(Usage::new()?)?;
    Ok(msg)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_heartbeat() {
        let hbmsg = create_heartbeat_message().unwrap();
        assert_eq!(hbmsg.topic, "HEARTBEAT".to_string());
        assert!(hbmsg.data["resources"]["utime"].as_f64().unwrap() > 0.0);
    }
}
